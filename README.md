# Help for curation process

**Disclaimer:** This software is distributed as it is. Please double-check the results.
If you encounter any bug/miss-correction, please put an issue on the gitlab page, or send pull-requests!

**This soft does not check 'patient age' column. Please check it by yourself**

## Requirements

Python3 with pip (pip3).


## How to get it?

Clone repository:

	git clone git@gitlab.pasteur.fr:aperrin/curation_tool.git

Then:

	cd curation_tool

To update, go to your 'curation_tool' folder, and type
	
	git pull

## How to install it

To install the software, and be able to call it from anywhere in your folders:

	pip3 install -e .

On the cluster, you may have problems with writing permissions. In that case, use:

	pip3 install --user -e .

Once installed, if a new version is created, you do not have to re-install. Just update the soft (see below, in 'How to run it').

## How to run it

I advise to always update the soft before using it, in case some modifications were done (which could be very often for this 
new-born software until it is more stable). For that, go to `curation_tool` folder, and type:

	git pull

To run the script only on metadata, use:

	curation_tool -m 'path to metadata xls file'

To run the full curation, use:
	
	curation_tool -m 'path metadata xls' -r 'path tsv server report' -s 'path fasta'

For more information, type:

	curation_tool -h



## What it does

### Metadata curation part

For each field, check if format is as expected (spaces in location, upper cases, 'unknown' in empty cells, 4,000x coverage format etc.). 

For date, converts it to text, with format YYYY-MM-DD. 

For fields like 'Location', 'Assembly' or 'Sequencing technology', it asks you to check that what is written is coherent. If not, give the correct value, and it will automatically change it. For example, in 'Location' we often see 'USA / Wyoming' instead of 'North America / USA / Wyoming'. 


### Sequence curation part

Then, it compares with the server report. If there is any problem requiring a sequence to be discarded, it removes it from the fasta file and from the curated xls file.

If there are comments, it copies them to the corresponding entry in curated xls.

It also informs user when he needs to double-check with a blast and/or mafft.



## Output

As an output of the script, you have the **currated xls file** and the **curated fasta file** that you can upload on the server.

**Warning** don't forget to check the `.contact_sub.log` file (see below) before uploading. There might be some sequences to double-check with blast and/or mafft to fully complete the curated xls file.

This `.contact_sub.log` is a file with what you have to **say to the submitter** (things that require 'Contact Submitter' in the guidelines). And if you can **release or not** the sequence:

- *WARNING* are for things to inform the submitter, but sequence can still be released.
- *ERROR* are for things to inform the submitter, but you need to hand him back the sequences (you will find the full list of sequences to hand-back and why in the `.hand_back.csv` file described below).


More info on output files:

- `<metadatafile.xls>.changes.log`: all changes done in the metadata
- `<metadatafile.xls>.contact_sub.log`: changes done that lead to submitter contact. Messages start with ERROR if sequence cannot be released, or WARNING if you have to inform submitter but can release the sequence. Also, this soft does not check 'patient age' column. Please check it by yourself.
- `<metadatafile.xls>.hand_back.csv`: csv file with all sequences to hand back, and why they have not made it through the curation.
- `<fasta_file.fa>.curated.fasta`: the new fasta file. If nothing changed, it is the same as the input fasta file. Otherwise, there can be changes like sequences renamed and/or removed because they need to be handed back.
- `<metadatafile.xls>.curated.xls`: the new xls file, with curated metadata


