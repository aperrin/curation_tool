#!/usr/bin/env python3
# coding: utf-8

#############################################################################
# This program provides some help for data curation                         #
#                                                                           #
# Authors: Amandine PERRIN                                                  #
# Copyright (c) 2020  Institut Pasteur, CNRS                                #
#                                                                           #
# This file is part of the curation program.                                #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as                   #
# published by the Free Software Foundation, either version 3 of the        #
# License, or (at your option) any later version.                           #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

import logging
logger = logging.getLogger("GC.server")

from curation_tool import common

def get_server_info(cov_report, to_hand_back, vname_changes):
    """
    Read server report

    cov_report: output file of server, in tsv format
    to_hand_back : {seq: reason}
    vname_changes: {new_name: ori_name}
    fasta_file: fasta filename if any
    """
    report = common.read_tsv(cov_report)
    # Empty cells
    report = report.fillna("")
    seen = False   # at each new line, seen = False. When something must be checked by the curator, put it to
    # True. Then, increment to_check only if seen = False. To avoid adding several times the same sequence if it has
    # several problems to check.
    to_check = 0  # number of entries to check by curator

    # For each "old" vname (which is in report), get corresponding new name (which is now in curated metadata)
    change_vnames = {old: new for (new, old) in vname_changes.items()}
    # We will check all lines, even those already tagged as to_hand_back.
    # -> add the reasons found here to hand back the sequences

    # Check all fields, line by line
    for index, line in report.iterrows():
        seen = False
        seq = line["Query"].strip().split("|")[0]  # seq is sequence name in server
        # Remove details on sequence name (acc number etc.) only keep virus name
        line["Query"] = seq
        # If seq ID was changed, get its new value
        if seq in change_vnames:
            seq = change_vnames[seq]  # seq is now the sequence name in xls

        comment = line["Comment"].strip()
        symbol = line["Symbol"].strip()

        # Get %N and %uniqmut, hand back if too high (%N>50), check that comment icon is ok (done bellow)
        line_perc_n = line["%N"]
        line_perc_uniqmut = line["%UniqueMuts"]
        perc_n = float(line_perc_n.strip().split("%")[0])
        perc_uniqmut = float(line_perc_uniqmut.strip().split("%")[0])

        # If frameshift, add sequence to hand_back ones, and go to next one
        if "frameshift" in comment.lower():
            if seq in to_hand_back:
                # If this sequence name was changed, put original sequence name
                if seq in vname_changes:
                    to_hand_back[vname_changes[seq]] += f" ; Comment: '{comment}'"
                else:
                    to_hand_back[seq] += f" ; Comment: '{comment}'"
            else:
                # If this sequence name was changed, put original sequence name
                if seq in vname_changes:
                    to_hand_back[vname_changes[seq]] = f"Comment: '{comment}'"
                else:
                    to_hand_back[seq] = f"Comment: '{comment}'"
            continue # no need to check more, this sequence will be put to hand back

        # If more than 50% NNNs, hand back sequence
        if perc_n > 50:
            # If already to hand back, just add this reason
            if seq in to_hand_back:
                reason = f" ; %N = {perc_n}, comment: '{comment}'"
                # If this sequence name was changed, put original sequence name
                if seq in vname_changes:
                    to_hand_back[vname_changes[seq]] += reason
                else:
                    to_hand_back[seq] += reason
            else:
                reason = f"%N = {perc_n}, comment: '{comment}'"
                # If this sequence name was changed, put original sequence name
                if seq in vname_changes:
                    to_hand_back[vname_changes[seq]] = reason
                else:
                    to_hand_back[seq] = reason
            continue # no need to check more, this sequence will be put to hand back

        # Check if missing gene. If yes, ask curator to check with mafft that it is due to 'N' or '-'
        if "missing" in comment.lower():
            if not seen:
                to_check += 1
            seen = True
            logger.warning(f"For {seq}, we have a missing gene. Please check with mafft: '{comment}'. "
                             "Then, remove missing mention from comment field in curated xls file.")

        # If strand negative, ask to double-check with blast
        if line["Strand"].lower() != "positive":
            if not seen:
                to_check += 1
            seen = True
            logger.warning(f"For {seq}, please double-check strand with blast. "
                            "If really negative, reverse-complement in fasta file, and then put 'positive' in curated metadata file.")

        # Check symbols
        if perc_n >=1 and perc_n < 5:
            if symbol.lower() != "warning":
                if seen:
                    to_check += 1
                seen = True
                if seq in vname_changes:
                    logger.error(f"Please check symbol for {seq} (called {vname_changes[seq]} before curation).")
                else:
                    logger.error(f"Please check symbol for {seq}.")

        # If there are more than 5% N, symbol must be alert. Comment does not change.
        if perc_n >= 5 and perc_n <= 50:
            if symbol != "Alert":
                line["Symbol"] = "Alert"
                logger.warning(f"For {seq}, according to %N, symbol was changed to alert")

        # If %uniqmut > 2, comment must be alert
        if perc_uniqmut > 2:
            if symbol != "Alert":
                line["Symbol"] = "Alert"
                logger.warning(f"For {seq}, according to %uniqmut, symbol was changed to alert")

        # Strip() all virus names
        line["Query"] = seq

    return report, to_check
