#!/usr/bin/env python3
# coding: utf-8


#############################################################################
# This program provides some help for data curation                         #
#                                                                           #
# Authors: Amandine PERRIN                                                  #
# Copyright (c) 2020  Institut Pasteur, CNRS                                #
#                                                                           #
# This file is part of the curation program.                                #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as                   #
# published by the Free Software Foundation, either version 3 of the        #
# License, or (at your option) any later version.                           #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

import sys
import pandas as pd


def read_xls(file_in):
    """
    Check if the given file respects the expected template. If not, error message and exit programm

    If some expected columns are missing, check if the sheets are not in the wrong order (try 1st sheet as metadata)
    In that case, swap both sheets.

    return
    	md = dataframe with metadatas
    	instructions = dataframe with instructions (we do not care about its content)
    """
    # Read input xls file
    try:
        md = pd.read_excel(file_in, sheet_name=1, header=0, dtype=str)
        instructions = pd.read_excel(file_in, sheet_name=0, header=0, dtype=str)
    # except TabError:
    #     print(f"ERROR: {file_in} not in expected format.")
    except FileNotFoundError:
        print(f"ERROR: '{file_in}' does not exist.", file=sys.stderr)
        sys.exit(1)
    except IndexError:
        print(f"ERROR: '{file_in}' not in expected format. Metadatas should be in the second sheet.", file=sys.stderr)
        sys.exit(1)
    except:
        print(f"ERROR: '{file_in}' is not an xls file.", file=sys.stderr)
        sys.exit(1)

    # Check if format is ok:
    column_headers = ["submitter", "fn", "covv_virus_name", "covv_type", "covv_passage", "covv_collection_date",
                      "covv_location", "covv_host", "covv_gender", "covv_patient_age", "covv_seq_technology",
                      "covv_assembly_method", "covv_coverage", "covv_orig_lab", "covv_orig_lab_addr",
                      "covv_subm_lab", "covv_subm_lab_addr", "covv_authors", "covv_comment", "comment_type"]

    # Check if all expected headers are in md.
    if not check_column_names(md, column_headers):
    	# If not all headers in md, swap md and instructions, and look into new md
    	md, instructions = instructions, md
    	found = check_column_names(md, column_headers)
    	# If not found in the other sheet either, error and exit
    	if not found:
    		print(f"ERROR: '{file_in}' not in expected format. Missing columns.", file=sys.stderr)
    		sys.exit(1)

    return md, instructions


def read_tsv(file_in):
    """
    Read tsv file provided by server

    """
    try:
        report = pd.read_csv(file_in, sep='\t', dtype=str)
    except FileNotFoundError:
        print(f"ERROR: '{file_in}' does not exist.", file=sys.stderr)
        sys.exit(1)
    except UnicodeDecodeError:
        print(f"ERROR: '{file_in}' is a binary file.", file=sys.stderr)
        sys.exit(1)

    # Expected column headers:
    column_headers = ["Query", "Strand", "%N", "%UniqueMuts", "Comment", "Symbol"]
    if not check_column_names(report, column_headers):
        print(f"ERROR: '{file_in}' not in expected format. Missing columns.", file=sys.stderr)
        sys.exit(1)
    return report


def check_column_names(df, column_headers):
    """
    Check column names of the dataframe.

    df: dataframe to check

    return
    	true if format ok, false otherwise
    """
    for column_header in column_headers:
        if column_header not in df.columns:
    		# Problem in md sheet format. Let's try with instructions sheet instead
            return False
    return True



def write_xls(sheet1, sheet2, file_out):
    """
    Write sheet1 and sheet2 in <file_out> xls file
    """
    writer = pd.ExcelWriter(file_out)
    sheet1.to_excel(writer, sheet_name='instructions', index=False)
    sheet2.to_excel(writer, sheet_name='Submissions', index=False)
    writer.save()


def write_tsv_from_dict(data_dict, out_file, sep="\t"):
    """
    write 'data_dict' to out_file in csv format (default sep = "\t")
    """
    with open(out_file, "w") as outf:
        for seq, reason in data_dict.items():
            outf.write(sep.join([seq, reason]) + "\n")
