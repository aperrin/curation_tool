#!/usr/bin/env python3
# coding: utf-8

#############################################################################
# This program provides some help for data curation                         #
#                                                                           #
# Authors: Amandine PERRIN                                                  #
# Copyright (c) 2020  Institut Pasteur, CNRS                                #
#                                                                           #
# This file is part of the curation program.                                #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as                   #
# published by the Free Software Foundation, either version 3 of the        #
# License, or (at your option) any later version.                           #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

import pandas as pd
import os
import sys
import logging
logger = logging.getLogger("GC.final")


def cure_fasta(fasta_file, cur_fasta_file, to_hand_back, new_old, vnames_list):
    """
    Change fasta file:
    - if sequences in to_hand_back: remove them
    - if sequence in virus_IDs: change its ID

    to_hand_back = {seq: reason} seq with old name
    new_old = {new_id: orig_id}
    vnames_list : list of new vnames
    """
    problem_sequence_IDs = False
    remain_vnames_list = vnames_list
    old_new = {old:new for new, old in new_old.items()}
    init_nb_seq = 0  # number of sequences in original fasta file
    final_nb_seq = 0  # number of sequences in fasta file

    with open(fasta_file, "r") as fasta, open(cur_fasta_file, "w") as nfasta:
        to_write = True
        for line in fasta:
            # Header line,
            if line.startswith(">"):
                init_nb_seq += 1
                # ex1 : line = >id|virus other info
                # ex2: >id2 other info2
                header_text = line.split(">")[-1].strip()
                # ex1 : "id|virus other info"
                # ex2: "id other info"
                header = header_text.split("|")[0].strip()
                # ori_header: keep header as written in fasta file
                ori_header = header
                # ex1: "id"
                # ex2: "id2"
                elems = header_text.split(header)
                # ex1: ["", "|virus other info"]
                # ex2: ["", "other info2"]
                # If header not found in vnames (header is original name), nor in changed vnames, log error and skip line
                if header not in vnames_list and header not in old_new:
                    logger.error(f"{header} entry in fasta file does not correspond to any vname. "
                                  "This sequence will be removed in the curated fasta file.")
                    problem_sequence_IDs = True
                    to_write = False
                    continue
                # Save header to new value, to, then, compare to ori_header
                new_header = header
                # If other elements in fasta entry, save them
                if len(elems) > 1:
                    elems = elems[1:]
                else:
                    elems = [""]

                # If header in hand_back (hand_back contains original vnames), do not write
                if header in to_hand_back:
                    to_write = False
                    # Now, remove header from remaining sequences.
                    # If header was changed, remove new name
                    if header in old_new:
                        header = old_new[header]
                        remain_vnames_list.remove(header)
                    # if header was not changed, remove curent name
                    else:
                        remain_vnames_list.remove(header)
                    continue  # go to next sequence, as this one will be removed

                # If header in changed vname, replace by new vname
                if header in old_new :
                    header = old_new[header]
                    remain_vnames_list.remove(header)

                # If header != ori_header, it means that it was in change_vnames. So, write new header
                if header != ori_header:
                    new_header = f">{header} {'|'.join(elems)}\n"
                    nfasta.write(new_header)
                    final_nb_seq += 1  # we are writting a new sequence
                    to_write = True
                # If no problem (not to_hand_back, and vname exists and did not change),
                # write line as it is
                else:
                    remain_vnames_list.remove(header)
                    nfasta.write(line)
                    final_nb_seq += 1  # we are writting a new sequence
                    to_write = True
            # Séquence line
            elif to_write:
                nfasta.write(line.replace("-", ""))
    # Write sequences missing in fasta file
    if remain_vnames_list:
        for vname in remain_vnames_list:
            if vname in new_old:
                logger.error(f"{vname} sequence (previously called {new_old[vname]}) is missing in fasta file. "
                              "This line will be removed in curated xls file.")
                problem_sequence_IDs = True
            else:
                logger.error(f"{vname} sequence is missing in fasta file. "
                              "This line will be removed in curated xls file.")
                problem_sequence_IDs = True

    else:
        print("WARNING: Please check that server comments have been copied to xls file "
              "(check that at least one is copied, you do not need to check all of them). This is a 'debug check': if not copied, "
              "please contact me (Amandine PERRIN).\n\n")

    # Return the list of vnames to remove from xls (because there are no corresponding fasta seqs) + number of seqs removed from fasta
    return remain_vnames_list, init_nb_seq, final_nb_seq


def complete_xls(md, report, to_hand_back, new_old, cur_fasta_file, remain_vnames_list):
    """
    From curated metadatas and server report, complete curated output file

    md : dataframe with all metadata curated. vnames are the new ones
    report : dataframe with comments and symbols to add to md
    to_hand_back : dict {vname: reason}
    new_old: dict {new_name: old_name}
    remain_vnames_list : list of xls vnames not found in fasta


    return
    out_md: dataframe with curated and completed metadata info
    hb: dataframe with lines of xls metadata removed
    nb_removed : number of lines removed from xls (because hand back, or no fasta sequence)
    """
    old_new = {old:new for new, old in new_old.items()}
    init_nb = 0 # number of lines in original xls file
    nb_removed = 0 # number of lines removed in curated xls file
    # Create new dataframe, where handed-back lines are put.
    # Also remove those lines from md.
    change_fasta = False
    hb = pd.DataFrame()  # to put all lines discarded
    out_md = md.copy()   # to put updated lines

    # For each line of the curated dataframe, add comment and symbol values
    for index, line in out_md.iterrows():
        seq = line["covv_virus_name"].strip()
        # If not 2nd header line, count as new line in xls
        if not "virus" in seq.lower():
            init_nb += 1
        # Get comment and symbol values of report for line where virusname = seq. report now has the new vname
        entry = report.loc[report.Query == seq]
        comment = entry["Comment"].values
        # If no corresponding line of this vname in server report, skip line
        if entry.empty and not seq.lower() == "virus name":
            continue
        # If comment not empty, and only 1 line with this virus name, copy comment in out_md
        if comment.size == 1:
            comment = comment[0]
            line["covv_comment"] = comment
        symbol = report.loc[report.Query == seq]["Symbol"].values
        if symbol.size == 1:
            symbol = symbol[0]
            line["comment_type"] = symbol
        # Write the name of curated fasta file in fn column
        # Don't write it for the 2n header column (FASTA filename header)
        if "FASTA" not in line["fn"]:
            line["fn"] = os.path.basename(cur_fasta_file)

    # Check if vname (new vname) in remain_vnames_list -> if yes, remove line
    # Remove lines which do not have a corresponding sequence in fasta file
    for vname in remain_vnames_list:
        # Remove line if it exists (not already removed by hand back for example)
        line = md.loc[md.covv_virus_name == vname]
        if not line.empty:
            nb_removed += 1
            out_md = out_md.drop(md[md.covv_virus_name == vname].index)


    # For each sequence to hand back, remove its line from md, and copy it to hand-back dataframe
    # hb_vname = vname in to_hand_back list, = original name
    for hb_vname in to_hand_back:
        # if hb_name has been changed, remove xls entry of corresponding new name
        vname = hb_vname
        if hb_vname in old_new:
            vname = old_new[hb_vname]
        line = md.loc[md.covv_virus_name == vname]
        # add line to handback dataframe
        # remove line from initial xls file if it exists
        if not line.empty:
            hb = hb.append(line)
            nb_removed += 1
            out_md = out_md.drop(md[md.covv_virus_name == vname].index)


    return out_md, hb, init_nb, nb_removed


