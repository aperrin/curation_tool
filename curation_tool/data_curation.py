#!/usr/bin/env python3
# coding: utf-8


#############################################################################
# This program provides some help for data curation                         #
#                                                                           #
# Authors: Amandine PERRIN                                                  #
# Copyright (c) 2020  Institut Pasteur, CNRS                                #
#                                                                           #
# This file is part of the curation program.                                #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as                   #
# published by the Free Software Foundation, either version 3 of the        #
# License, or (at your option) any later version.                           #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

import sys
import pandas as pd
import unidecode
import string

import logging
logger = logging.getLogger("GC.metadata")

from curation_tool import utils
from curation_tool import common


def cure_metadata(file_in):
    """
    file_in in xls format
    """
    # dict to put {original_value: new_value} (new_value can be the same as original one)
    # to avoid re-checking next time we see this value
    locations_list = {}
    dates_list = {}
    details_list = {"original":"Original", "vero":"Vero"}  # Original and Vero are OK
    hosts_list = {}
    countries = {}  # countries found in virus names.
    genders_list = {}
    covs_list = {}
    orilab_list = {}
    orilabaddress_list = {}
    sublab_list = {}
    sublabaddress_list = {}
    assembly_list = {}
    seqtechno_list = {"illumina miseq": "Illumina MiSeq", "sanger": "Sanger",
                      "nanopore minion": "Nanopore MinION", "ion torrent": "Ion Torrent"}
    authors_list = {}
    vnames_list = []  # list of virus names

    vname_changes = {}  # {new_name: original_name} for virusname that were changed.

    # Sequences to remove, and why
    to_hand_back = {}  # seq_id: reason

    cov_problem = False  # Set to True if we have a problem with a coverage.
    # After that, other problems will only be written in changes.log, to avoid overflow in
    # contact_sub.log

    # Sometimes, assembly method was incremented by a bad "Excell fill down" by the user. Hence,
    # it will always ask if method is ok (as these are different methods each time)
    # Curator can skip this column
    skip_assembly = False
    # same as for assembly method
    skip_seqtechno = False
    skip_authors = False

    # Read xls file, checking that it corresponds to the expected template
    md, instructions = common.read_xls(file_in)
    # Empty cells
    md = md.fillna("")
    # Check all fields, line by line
    for index, line in md.iterrows():
        # Skip 2nd header line
        if "filename" in line["fn"]:
            continue
        # Check covv_type.
        check_type(line)
        # Check location field
        check_location(line, locations_list)
        # Check virus names
        check_vnames(line, vnames_list, countries, to_hand_back, vname_changes)
        # Check dates
        check_date(line, dates_list)
        # Check passage history/details column
        check_column(line, "covv_passage", details_list, capital=True)
        # Check host
        check_column(line, "covv_host", hosts_list, capital=True)
        # Check gender
        check_gender(line, genders_list)
        # Check originating and submitting lab and address
        # If not given, contact submitter and DO NOT release
        check_mandatory_field(line, "covv_orig_lab", orilab_list, to_hand_back, alert=True)
        check_mandatory_field(line, "covv_orig_lab_addr", orilabaddress_list, to_hand_back,
                              alert=True)
        check_mandatory_field(line, "covv_subm_lab", sublab_list, to_hand_back, alert=True)
        check_mandatory_field(line, "covv_subm_lab_addr", sublabaddress_list, to_hand_back,
                              alert=True)
        if not skip_authors:
            skip_authors = check_mandatory_field(line, "covv_authors", authors_list, to_hand_back,
                                                 alert=False, user_check=True)
        if not skip_seqtechno:
            # same as for assembly_method
            # Possible answers: Illumina MiSeq / Sanger / Nanopore MinION / Ion Torrent
            # (any extra info in sequencing technology place them in parenthesis.
            skip_seqtechno = check_seq_techno(line, seqtechno_list)
        # Check coverage
        cov_problem = check_coverage(line, covs_list, cov_problem)

    logger.warning("TO CURATOR: please check 'Patient age', as it is not checked by this tool.")

    return instructions, md, to_hand_back, vname_changes, vnames_list



def check_type(line):
    """
    type must always be 'betacoronavirus'

    Parameters
    ----------
    line: pandas.core.series.Series
        line currently checked
    """
    vtype = line["covv_type"]
    if vtype != "betacoronavirus":
        logger.info(f"Type changed from '{vtype}' to 'betacoronavirus'.")
        line["covv_type"] = "betacoronavirus"


def check_location(line, locations):
    """
    Check Location column

    line = pandas.core.series.Series
    locations -> dict {prev_loc: new_loc}
    """
    write_warning = True
    ori_location = line["covv_location"].strip()
    location = ori_location
    location_ok = False

    # If we already saw this field, and it was valid, just skip checking this time
    if location in locations:
        location = locations[location]
        line["covv_location"] = location
        locations[ori_location] = location
        location_ok = True
        return

    # Checl location format
    new_sep = checked_location_format(location, locations)
    location = " / ".join(new_sep)

    # If location was changed, inform curator.
    # If it was changed exactly as before, no need to write it
    if ori_location.strip() != location.strip():
        logger.info(f"'Location' column: changed '{ori_location}' to '{location}'.")

    line["covv_location"] = location
    locations[ori_location] = location


def checked_location_format(location, locations):
    """
    Check if Location is in expected format: Continent / Country / Region

    location -> str
    locations -> dict {prev_loc: new_loc}
    """
    # First, check that location given contains non empty strings between the '/'
    fields_ok = False

    write_warning = True

    # Now, check that first field, continent, is among this list
    continents = ["europe", "asia", "africa", "north america",
                  "south america", "oceania", "central america"]
    new_loc = location

    while not fields_ok:
        sep = new_loc.strip().split("/")
        # remove accents and trailing spaces
        new_sep = [unidecode.unidecode(f.strip()) for f in sep]
        new_loc = " / ".join(new_sep)
        # If empty field, ask for new location
        if empty_field(sep) or len(sep) < 2:
            print("------LOCATION checking-----")
            print(f"Wrong format for location: {new_loc}. ")
            new_loc = input("Please enter correct location, in format 'Continent / Country [/ Region]'\n")
            # Restart while with new location value
            continue
        # Check that continent is ok
        if not new_sep[0].lower() in continents:
            print("------LOCATION checking-----")
            print(f"Wrong format for location: {new_loc}. ")
            answer = input("First field must be a continent. Pick one among:'\n"
                           "Europe (1), Asia (2), Africa (3), North America (4), South America (5), "
                           "Oceania (6) or Central America (7):\n")
            if answer.isdigit() and int(answer) in range(1,8):
                continent = string.capwords(continents[int(answer) - 1])
                new_sep = [continent] + new_sep
            elif answer in continents:
                continent = string.capwords(answer)
                new_sep = [continent] + new_sep
            new_loc = " / ".join(new_sep)
            continue # go back to while, with new value of location
        else:
            new_sep[0] = string.capwords(new_sep[0])
            new_loc = " / ".join(new_sep)
        # Format is ok, but it was automatically changed (remove accents, trailing characters...)
        if new_loc != location:
            print("------LOCATION checking-----")
            print(f"'{location}' was changed to {new_loc}.")
            write_warning = False
        # If warning comes from a change, the 'LOCATION checking' flag was already printed.
        # If not (just asking user confirmation), print 'LOCATION checking' here
        if write_warning:
            print("------LOCATION checking-----")
        answer = input(f"Is location '{new_loc}' ok? ([Y]/n)")
        # If user says no, ask him for new location value
        if answer.lower() in ['n', 'no']:
            new_loc = input("Please enter correct location, in format 'Continent / Country [/ Region]'\n")
            while not new_loc:
                new_loc = input("Please enter correct location, in format 'Continent / Country [/ Region]'\n")
        # If user says yes: OK for location
        if not answer or answer.lower() in ["y", "yes"]:
            fields_ok = True

    # First field = continent. Format ok
    # Other fields: start with upper letter. Then, keep other letters as given by user
    new_sep[1:] = [f[0].upper() + f[1:] if len(f)>1 else f[0].upper() for f in new_sep[1:] ]
    return new_sep


def empty_field(sep):
    """
    sep: list of string, location split at each '/'
    Check if all fields of given location are not empty.
    -> ["Europe", ""] -> True
    -> ["europe", "france"] -> False
    """
    for f in sep:
        if not f:
            return True
    return False


def check_vnames(line, vnames_list, countries, to_hand_back, vname_changes):
    """
    line = pandas.core.series.Series
    vnames_list : list of virus names
    vname_changes : dict {new_name:old_name}
    """
    # Get given virus name, and given location (to compare with 2nd field of virus name)
    vname = line["covv_virus_name"].strip()
    location = line["covv_location"].strip()

    fields = vname.strip().split("/")
    fields = [f.strip() for f in fields]

    # If vname already exists : problem
    uniq = False
    orig_vname = vname   # same original virus name given
    # Remove possible accents in virus name
    vname = unidecode.unidecode(vname)

    # If virus name already exists, put sequence to hand_back list.
    # We could ask for a new name, but we cannot tell which fasta sequence corresponds to 1 or the other metadata entry.
    # So, hand back to submitter telling that he had twice the same virus name.
    if vname in vnames_list:
        to_hand_back[vname] = "Duplicated virus name."
        return

    # We are now sure that the virus name is unique. Let's check if it has the 4 required fields.
    fields_ok = False
    while not fields_ok:
        if len(fields) == 4:
            fields_ok = True
        else:
            print("\n------VIRUS NAME checking-----")
            print(f"'{vname}' is not a valid virus name. It should follow this format: "
                  "hCoV-19/Country/Identifier/2020.")
            answer = input("Please give correct virus name, or type 'STOP' "
                           "to stop program and go back yourself to the xls file.\n")
            if answer == "STOP":
                sys.exit(1)
            fields = answer.strip().split("/")
            fields = [f.strip() for f in fields]
            vname = answer

    # We are now sure that virus name is uniq, and there are 4 fields
    # Fields required for a virus name
    final_vname = checked_vname_format(vname, location, countries)
    if final_vname not in vnames_list:
        vnames_list.append(final_vname)
    # Log if we changed something
    if orig_vname != final_vname:
        vname_changes[final_vname] = orig_vname
        logger.warning(f"Changed sequence name '{orig_vname}' to '{final_vname}'.")

    line["covv_virus_name"] = final_vname


def checked_vname_format(vname, location, countries):
    """
    Check if vname is in expected format: hCoV-19/Country/Identifier/2020

    vname -> str
    vnames -> list of virus names
    location : str
    countries: {ori_country_in_vname: new_country_in_vname}
    """
    name = "hCoV-19"
    country = ""
    v_id = ""
    date = "2020"
    fields = vname.split("/")

    # Check country is in location. Otherwise, get country
    if fields[1].upper() not in location.upper():
        # Country not in location, but already seen before -> replace as it
        # has been replaced before
        countries_lower = [c.lower() for c in countries.keys()]
        if fields[1].lower() in countries_lower:
            country = countries[fields[1]]
        # Country not in location and never seen before: try to fix it
        else:
            country = location.split("/")[1].strip()
            print("\n------VIRUS NAME checking-----")
            print(f"Problem with '{vname}' virus name. It should follow this format: "
                   "hCoV-19/Country/Identifier/2020. Here, 'Country' field does not correspond "
                   "to what is given in 'Location' column. We took the correct country directly "
                   f"from this 'Location' column: {country}.")
            answer = input(f"\t * 'Y' (default) if you are ok with this new value '{country}' in the virus name\n"
                           f"\t * 'n' if you still want to keep the original value ({fields[1]})\n"
                           f"\t * '<new value>' text to put if you want neither '{country}' nor '{fields[1]}'\n")
            # no: keep fields[1], do not change country. Save this for next time
            if answer.lower() in ["n", "no"]:
                country = fields[1]
                countries[country] = country
            elif not answer or answer.lower() in ['y', 'yes']:
                countries[fields[1]] = country
            else:
                countries[fields[1]] = answer
                country = answer

    # If country field corresponds to location column, keep it as is
    else:
        country = fields[1]

    # Get virus ID
    v_id = fields[2]
    final_vname = "/".join([name, country, v_id, date])
    # Will put name and date by default. If it was different before, then final_vname
    # will be different from vname, and we will log this change
    return final_vname


def check_column(line, column, column_list, capital=False):
    """
    First letter must be uppercase, others lowercase.
    if details/history, ask curator to confirm. Sometimes, it is written like 'clinical sample'.
    Should be replaced by 'Original (clinical sample)'

    line = pandas.core.series.Series
    column : str, header of column to check
    column_list : dict {ori_text:new_text}

    Works for
    - passage details/history
    - host
    """
    text_ok = False
    column_text = line[column].strip()
    seq = line["covv_virus_name"].strip()

    final_column_text = column_text
    while not text_ok:
        # If we already saw this field, and it was valid, just skip checking this time
        if column_text.lower() in column_list:
            final_column_text = column_list[column_text.lower()]
            text_ok = True
        # If empty or UNKNOWN: put 'unknown'
        elif not column_text or column_text.lower() == "unknown":
            final_column_text = "unknown"
            column_list[column_text] = "unknown"
            text_ok = True
        else:
            # If column is covv_passage, we will ask user to confirm. If he confirms,
            # answer ok will be True. Otherwise, False -> recheck
            # Ask to check if cov_passage:
            answer_ok = True  # Did the curator answer yes, or a new text
            if column == "covv_passage":
                print("------PASSAGE DETAILS/HISTORY checking-----")
                answer = input(f"Is '{column_text}' ok for 'Passage details/history'? "
                                "It should start with Original or Vero. Y/new_text:\n" )
                # If user answered yes, we keep what we already have
                if not answer or answer.lower() in ["y", "yes", ""]:
                    final_column_text = column_text
                # If answer is a new string (not 'no'), this is the new text
                # User should not answer 'no', but let's check
                elif answer and answer.lower() not in ["no", "n"]:
                    final_column_text = answer
                else:
                    answer_ok = False
                # If he answered no, will be asked again
            # If user confirmed or gave new text, format text and release
            if answer_ok:
                # Put first letter in upper case if asked
                if capital:
                    final_column_text = final_column_text.capitalize()
                text_ok = True

    if not final_column_text in column_list:
        column_list[column_text.lower()] = final_column_text
    if final_column_text != column_text:
        logger.info(f"For {seq}, '{column}' column: changed '{column_text}' to '{final_column_text}'.")
        # Update information
        line[column] = final_column_text


def check_mandatory_field(line, column, column_list, to_hand_back, alert=False, user_check=False):
    """
    For a mandatory field, check that there is something in it. If not, ask submitter
    to fill it.

    Works for:
    - originating lab (alert=True, user_check=False)
    - address originating lab (alert=True, user_check=False)
    - submitting lab (alert=True, user_check=False)
    - address submitting lab (alert=True, user_check=False)
    - assembly (alert=False, user_check=True)
    - authors (alert=True, user_check=True)

    line: whole line
    column: header of column
    column_list: {text: new_text} for each 'text' already seen and checked
    to_hand_back: dict {seq_ID: reason why we cannot release it}
    alert: true: if info empty or unknwon, or curator says that it is not ok:
                 curator must contact submitter AND NO release.
                 ex: orig_lab, orig_lab_address, sub_lab, sub_lab_address
           false: if info empty or unknown, or curator says that it is not ok:
                  contact submitter but can release
    user_check: true: if not empty or unknown, ask user if this text is ok or not.
                      User can answer 's' to skip this column starting from this sequence.
                false: if not empty or unknown, just keep text, do not ask user. (so, no possibility to skip)

    return bool: if column will be skipped after or not

    """
    text = line[column].strip()
    seq = line["covv_virus_name"].strip()

    new_text = text

    # Checkpoint 1:
    # already seen and checked
    if text in column_list:
        # if text is unknown, and alert, remove sequence
        if text == "unknown" and seq in to_hand_back and alert:
            to_hand_back[seq] += f" ;  {column} is unknown"
        elif text == "unknown" and alert:
            to_hand_back[seq] = f"{column} is unknown."
        new_text = column_list[text]
        line[column] = new_text
        return False

    # Checkpoint 2:
    # If new text is empty, or filled with unknown, and it is the first time we are
    # in this case (did not end with first check point), write warning, or error according to
    # 'alert' flag
    if not new_text or new_text.lower() == "unknown":
        if alert:
            if seq in to_hand_back:
                    to_hand_back[seq] += f" ; {column} is unknown"
            else:
                to_hand_back[seq] = f"{column} is unknown."
            new_text = "unknown"
        else:
            logger.warning(f"{seq} has no {column} text. Filled to unknown. "
                           "Inform submitter he could give this information. Can be released.")
            new_text = "unknown"

    # Checkpoint 3:
    # If there is something (not unknown), ask user_check = True
    # Ask user if this text is ok or not
    if new_text != "unknown" and user_check:
        print(f"\n------{column.upper()} checking-----")
        answer = input(f"For seq '{seq}', is '{new_text}' fine for column {column}? \nAnswers:\n"
                        "\t* 'Y' (default) to accept this text. Following lines with the "
                        "same text will be directly considered as 'OK'\n"
                        "\t* 'no': this text is not correct. Keep it as is, but write "
                        "a warning in the contact_submitter file. As for 'y', following lines with the "
                        "same text will be directly considered as 'OK'\n"
                        "\t* <new_value>: text to put instead of current one\n"
                        "\t* 's': to skip checking this column starting from this sequence. "
                        "Whatever the content of the following lines, it will be kept as is, "
                        "and you can check it yourself after.\n")
        # user asks to ignore this column starting from this line
        if answer.lower() in ['s', 'skip']:
            logger.error(f"TO CURATOR: You skipped {column} starting from sequence {seq}. Please check it "
                         "yourself.")
            return True
        # if user said no, write warning or alert, and keep same text
        elif answer.lower() in ['no', 'n']:
            if alert:
                if seq in to_hand_back:
                    to_hand_back[seq] += f" ;  {column} is wrong"
                else:
                    to_hand_back[seq] = f"{column} is wrong."
            else:
                logger.warning(f"Sequence {seq}: Wrong text for {column} column. Ask submitter." )
        # User entered a value:
        # if not yes and not no (and not skip), new_text = new_value
        elif answer.lower() not in ['y', 'yes', '', 'n', 'no']:
            new_text = answer
        # if user said yes -> keep new_text (as for 'no', but without any warning)

    if text != new_text:
        logger.info(f"For sequence {seq}, '{column}' column: changed '{text}' to '{new_text}'.")
        # Update information
    column_list[text] = new_text
    line[column] = new_text
    return False


def check_seq_techno(line, technos_list):
    """
    Checking sequencing technology

    Possible answers: Illumina MiSeq / Sanger / Nanopore MinION / Ion Torrent
    (any extra info in sequencing technology place them in parenthesis.

    line = pandas.core.series.Series
    technos_list = {ori_value: changed_value}
    """
    default_technos = ["Illumina MiSeq", "Sanger", "Nanopore MinION", "Ion Torrent"]
    techno = line["covv_seq_technology"].strip()
    seq = line["covv_virus_name"].strip()

    new_techno = techno
    lower_technos_list = {o.lower():n for o, n in technos_list.items()}
    # Checkpoint 1:
    # already seen and checked
    if techno.lower() in lower_technos_list:
        new_techno = technos_list[techno.lower()]
        line["covv_seq_technology"] = new_techno
        return False

    # Checkpoint 2:
    # If new text is empty, or filled with unknown, and it is the first time we are
    # in this case (did not end with first check point), write warning, or error according to
    # 'alert' flag
    if not new_techno or new_techno.lower() == "unknown":
        logger.warning(f"{seq} has no 'Sequencing technology'. Filled to unknown. "
                       "Inform submitter he could give this information. Can be released.")
        new_techno = "unknown"

    # Checkpoint 3:
    # If there is something (not unknown and not already seen), ask user if this text is ok or not
    if new_techno != "unknown":
        print(f"\n------SEQUENCING TECHNOLOGY checking-----")
        str_technos = ", ".join(default_technos)
        answer = input(f"For seq '{seq}', is '{new_techno}' fine as a Sequencing technology? \n"
                       f"It should be something among:  {str_technos}. "
                        "Any extra info in sequencing technology should be placed in parenthesis.\nAnswers:\n"
                        "\t* 'Y' (default) to accept this text even if it is not in the default list. Following lines with the "
                        "same text will be directly considered as 'OK'\n"
                        "\t* 'no': this text is not correct. Keep it as is, but write "
                        "a warning in the contact_submitter file. As for 'y', following lines with the "
                        "same text will be directly considered as 'OK'\n"
                        "\t* <new_value>: text to put instead of current one\n"
                        "\t* 's': to skip checking this column starting from this sequence. "
                        "Whatever the content of the following lines, it will be kept as is, "
                        "and you can check it yourself after.\n")
        # user asks to ignore this column starting from this line. Write error and return True (skip seq techno column)
        if answer.lower() in ['s', 'skip']:
            logger.error(f"TO CURATOR: You skipped Sequencing technology starting from sequence {seq}. Please check it "
                         "yourself.")
            return True
        # if user said no, write warning or alert, and keep same text
        elif answer.lower() in ['no', 'n']:
            logger.warning(f"Sequence {seq}: Wrong text for Sequencing technology column. Ask submitter." )
        # User entered a value:
        # if not yes and not no (and not skip), new_text = new_value
        elif answer.lower() not in ['y', 'yes', '', 'n', 'no']:
            new_techno = answer
        # if user said yes (answer == '' or answer.lower() in ['y', 'yes'] -> keep new_text (as for 'no', but without any warning)
    if techno != new_techno:
        logger.info(f"For sequence {seq}, Sequencing technology column: changed '{techno}' to '{new_techno}'.")
        # Update information
    technos_list[techno.lower()] = new_techno
    line["covv_seq_technology"] = new_techno
    return False


def check_date(line, dates_list):
    """
    line = pandas.core.series.Series
    """
    # Save original field to write changes if there are
    ori_date = line["covv_collection_date"].strip()
    new_date = ori_date
    # Try to convert date to string, if it was in date format in excel
    try:
        # if complete date: written as 2020-03-01 00:00:00
        # if YYYY-MM -> written as is, so no need to change
        if "00:00:00" in new_date:
            new_date = str(pd.to_datetime(ori_date, yearfirst=True).strftime("%Y-%m-%d"))
    # If not able to convert, date is already a string, so stay as it is, and it will be checked as a string
    except:
        pass
    new_date = unidecode.unidecode(new_date)
    seq = line["covv_virus_name"].strip()
    # If we already saw and checked this, reuse what has been done
    if new_date in dates_list:
        line["covv_collection_date"] = dates_list[new_date]
    # If no date given, put unknown (empty field is 'NA filled up')
    if not new_date or new_date.lower() == "unknown":
        line["covv_collection_date"] = "unknown"
        dates_list[new_date] = "unknown"
        return

    # Date given and never seen before: check format
    correct_format = False
    numbers = new_date
    date = new_date
    while not correct_format:
        try:
            # get year, month, day
            numbers = numbers.split("-")
            # Try to convert each field to int. If it does not work -> error in date format
            numbers_int = [int(n.strip()) for n in numbers]
            if len(numbers) == 0 or len(numbers) > 3:
                raise ValueError
            if len(str(numbers[0])) != 4:
                raise ValueError("Year not in YYYY format")
            if len(numbers) > 1 and len(str(numbers[1])) != 2:
                raise ValueError("Month not in MM format")
            if len(numbers)> 2 and len(str(numbers[2])) != 2:
                raise ValueError("Day not in DD format")
            correct_format = True
        except ValueError as e:
            print("------COLLECTION DATE checking-----")
            numbers = input(f"For sequence {seq}, wrong format for collection date: {date}. \n"
                            "Please enter correct collection date in YYYY or "
                            "YYYY-MM or YYYY-MM-DD format:\n")
            new_date = numbers
    str_numbers = [str(n) for n in numbers]
    date_ok = "-".join(str_numbers)
    dates_list[ori_date] = date_ok
    line["covv_collection_date"] = date_ok
    if date_ok != ori_date:
        logger.info(f"'Collection date' column: changed '{ori_date}' to '{date_ok}'.")


def check_gender(line, genders_list):
    """
    check gender: must be Male, Female or unknown
    """
    gender_checked = False
    ori_gender = line['covv_gender'].strip()
    seq = line["covv_virus_name"].strip()
    gender = ori_gender
    if gender in genders_list:
        line['covv_gender'] = genders_list[gender]
        return
    while not gender_checked:
        if not gender or "unknown" in gender.lower() or "u" in gender.lower():
            final_gender = "unknown"
            gender_checked = True
        elif gender == "Female" or gender == "Male":
            final_gender = gender
            gender_checked = True
        elif gender.lower() in ["m", "male"]:
            final_gender = "Male"
            gender_checked = True
        elif gender.lower() in ["f", "female"]:
            final_gender = "Female"
            gender_checked = True
        else:
            print("------GENDER checking-----")
            gender = input(f"For {seq}, wrong format for gender: {gender}. \n"
                            "Please enter Male (m), Female (f) or unkown (u) (default)\n")
    if final_gender != ori_gender:
        logger.info(f"For {seq}, 'Gender' column: changed '{ori_gender}' to '{final_gender}'.")
    line['covv_gender'] = final_gender
    genders_list[ori_gender] = final_gender


def check_coverage(line, cov_list, cov_problem):
    """
    Check coverage format:
    * ',' if > 1000
    * lower 'x' after the number

    Allowed coverage: 1,000x / 1,000x (average) / >1,000x / 0.837354625522x

    If not given: unknown, contact submitter but release

    line: pandas.core.series.Series
    cov_list: {ori_cov: new_cov}
    cov_problem: True if we already saw a problem in coverage. Just write an info message
                      to inform that this coverage has been changed too
                 False if we never saw any coverage probleme: warning message +
                      'see .changes for more details'

    Returns cov_problem for next time
    """
    cov_ok = False
    ori_cov = line["covv_coverage"].strip()  # keep original value
    seq = line["covv_virus_name"].strip()
    cov = ori_cov

    if ori_cov in cov_list:
        cov = cov_list[ori_cov]
        cov_ok = True

    while not cov_ok:
        # If empty, fill with unknown and contact submitter
        if not cov or cov.lower() == "unknown":
            cov = "unknown"
            cov_ok = True
        # First, keep only coverage number (no 'x')
        else:
            cov_number = cov.lower().strip().split('x')[0]
            # If several ',' it is clear that they are separating hundreds and thousands (and we will anyway
            # probably not see it as this would be a very high coverage).
            # If only 1 ',' in value given, check that it is separating hundreds and thousands, and not
            # that it is used in place of a '.':
            if cov_number.count(",") == 1 and len(cov_number.split(",")[-1].strip()) != 3:
                # ',' is used instead of '.' : replace by "." and continue
                replaced_cov = cov_number.replace(",",".")
                if not cov_problem:
                    logger.warning(f"Coverage was changed for at least 1 sequence, because of a format error.")
                    cov_problem = True
                logger.info(f"{cov} was replaced by {replaced_cov} for sequence {seq}.")
                cov_number = replaced_cov
            # At this stage, there can be '.' separating decimals, or ',' separating hundreds and thousands.
            # Let's now check that everything between them is a number
            try:
                # if given value, without the ',' is an int -> format coverage with that
                cov_number = cov_number.replace(",", "")
                if cov_number.isdigit():
                    int_cov = int(cov_number)
                    cov = f"{int_cov:,}x"
                    cov_ok = True
                # If not an int, try if it is a float.
                # If yes -> format with that
                # If no, raise valueerror and ask curator
                else:
                    float_cov = float(cov_number)
                    cov = f"{float_cov:,}x"
                    cov_ok = True
                # Check that coverage is not too low
                if int(float(cov_number)) <= 2:
                    if not cov_problem:
                        logger.warning(f"For sequence {seq}, coverage is very low: {cov}! "
                                        "See changes.log to know if other sequences have low "
                                        "coverage too.")
                    logger.info(f"For sequence {seq}, coverage is very low: {cov}!")
            except ValueError as e:
                # Cov is not an int nor a float: ask curator to put the correct value.
                print("------COVERAGE checking-----")
                cov = input(f"Given coverage ({cov}) for {seq} is not a number.\n"
                             "  * If you are nethertheless ok with this value, type y or just press Enter.\n"
                             "  * If you want to replace with unknown, type 'u'\n"
                             "  * If you want to modify this coverage, type the new value. "
                             "Warning: this value will be considered as valid for the field, it will not be checked.\n")
                # Replace with unknown
                if cov.lower() == "u":
                    cov = "unknown"
                # Keep initial coverage value. No more checking -> cov_ok = True
                elif not cov or cov.lower in ['y', 'yes']:
                    cov = ori_cov
                    cov_ok = True
                # Take the string given by the user as the coverage value. No more checking -> cov_ok = True
                else:
                    cov_ok = True


    # If there was something in coverage but it was changed, log it
    # If it was empty, we just replaced by unknown. Already logged.
    if ori_cov != cov and ori_cov:
        if not cov_problem:
            logger.warning(f"At least 1 coverage was changed because of format error. "
                            "See '.changes.log' file for more information.")
            cov_problem = True
        logger.info(f"For {seq} 'Coverage' column: changed '{ori_cov}' to "
                       f"'{cov}'. Sequence can be released")
    elif not ori_cov:
        # If we never had a problem with coverage before, write warning message (in .contact_sub.log)
        # to refer to .changes.log for more details
        if not cov_problem:
            logger.warning(f"At least one coverage was not given. It was filled with unknown. "
                            "See '.changes.log' for more details. (Sequence can be released)")
            cov_problem = True
        # In any case, write changes information in .changes.log
        logger.info(f"For {seq}, coverage not given. Filled with unknown "
                    "(Sequence can be released)")
    cov_list[ori_cov] = cov
    line["covv_coverage"] = cov
    return cov_problem



if __name__ == '__main__':
    parsed = utils.make_parser(sys.argv[1:])
    metadata = parsed.xls_file
    logger = utils.init_logger(metadata, "curation_tool")
    cure_metadata(metadata)
